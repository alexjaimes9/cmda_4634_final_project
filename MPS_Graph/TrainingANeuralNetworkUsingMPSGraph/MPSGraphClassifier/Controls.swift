/*
See the LICENSE.txt file for this sample’s licensing information.

Abstract:
Some control paramters shared across the classifier
*/

import UIKit

// Training constants
let numTrainingIterations = 300
// Learning rate
let lambda = 0.01
// Original was 16
let batchSize: UInt = 16

// UI constants
let backgroundGray = #colorLiteral(red: 0.1607843137, green: 0.1647058824, blue: 0.1882352941, alpha: 1)
let barGreen = #colorLiteral(red: 0, green: 0.4705882353, blue: 0, alpha: 1)
