/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A shader that adds two arrays of floats.
*/

#include <metal_stdlib>
#include "../Loki/loki_header.metal"
using namespace metal;
/// This is a Metal Shading Language (MSL) function equivalent to the add_arrays() C function, used to perform the calculation on a GPU.
kernel void add_arrays(device const float* inA,
                       device const float* inB,
                       device float* result,
                       uint index [[thread_position_in_grid]])
{
    // the for-loop is replaced with a collection of threads, each of which
    // calls this function.
    result[index] = inA[index] + inB[index];
}

// Estimate the constant pi
kernel void estimate_pi(device float* result,
                        uint index [[thread_position_in_grid]]) {
    
    // The generator with the index as the seed
    Loki generator = Loki(index);
    
    float x;
    float y;
    // Keep track of the number of successes
    int hits = 0;
    // Each thread does a number of iterations
    for (int i = 0; i < 1000; i++) {
        // Generate the numbers
        x = generator.rand();
        y = generator.rand();
        // Check if it is inside the bounds
        if (x*x + y*y < 1) {
            hits += 1;
        }
    }
    // Write out the number of successes
    result[index] = hits;
}
