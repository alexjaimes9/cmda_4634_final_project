/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A class to manage all of the Metal objects this app creates.
*/

#import "MetalCalculator.h"

// The number of floats in each array, and the size of the arrays in bytes.
const unsigned int arrayLength = 1 << 16; // 16,777,216
const unsigned int bufferSize = arrayLength * sizeof(float);

@implementation MetalCalculator
{
    id<MTLDevice> _mDevice;

    // The compute pipeline generated from the compute kernel in the .metal shader file.
    id<MTLComputePipelineState> _mEstimateFunctionPSO;

    // The command queue used to pass commands to the device.
    id<MTLCommandQueue> _mCommandQueue;

    // Buffers to hold data.
//    id<MTLBuffer> _mBufferA;
//    id<MTLBuffer> _mBufferB;
    id<MTLBuffer> _mBufferResult;

}

- (instancetype) initWithDevice: (id<MTLDevice>) device
{
    self = [super init];
    if (self)
    {
        _mDevice = device;

        NSError* error = nil;

        // Load the shader files with a .metal file extension in the project

        id<MTLLibrary> defaultLibrary = [_mDevice newDefaultLibrary];
        if (defaultLibrary == nil)
        {
            NSLog(@"Failed to find the default library.");
            return nil;
        }

        // The function from metal
        id<MTLFunction> estimateFunction = [defaultLibrary newFunctionWithName:@"estimate_pi"];
        if (estimateFunction == nil)
        {
            NSLog(@"Failed to find the estimation function.");
            return nil;
        }
        
        // The function from metal
//        id<MTLFunction> addFunction = [defaultLibrary newFunctionWithName:@"add_arrays"];
//        if (addFunction == nil)
//        {
//            NSLog(@"Failed to find the adder function.");
//            return nil;
//        }

        // Create a compute pipeline state object.
        _mEstimateFunctionPSO = [_mDevice newComputePipelineStateWithFunction: estimateFunction error:&error];
        if (_mEstimateFunctionPSO == nil)
        {
            //  If the Metal API validation is enabled, you can find out more information about what
            //  went wrong.  (Metal API validation is enabled by default when a debug build is run
            //  from Xcode)
            NSLog(@"Failed to created pipeline state object, error %@.", error);
            return nil;
        }
//        else {
//            printf("Got here!");
//        }

        _mCommandQueue = [_mDevice newCommandQueue];
        if (_mCommandQueue == nil)
        {
            NSLog(@"Failed to find the command queue.");
            return nil;
        }
    }

    return self;
}

- (void) prepareData
{
    // Allocate a buffer to hold our initial data and the result.
//    _mBufferA = [_mDevice newBufferWithLength:bufferSize options:MTLResourceStorageModeShared];
//    _mBufferB = [_mDevice newBufferWithLength:bufferSize options:MTLResourceStorageModeShared];
    _mBufferResult = [_mDevice newBufferWithLength:bufferSize options:MTLResourceStorageModeShared];

//    [self generateRandomFloatData:_mBufferA];
//    [self generateRandomFloatData:_mBufferB];
}

- (void) sendComputeCommand
{
    // Create a command buffer to hold commands.
    id<MTLCommandBuffer> commandBuffer = [_mCommandQueue commandBuffer];
    assert(commandBuffer != nil);

    // Start a compute pass.
    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    assert(computeEncoder != nil);

    [self encodeEstimateCommand:computeEncoder];

    // End the compute pass.
    [computeEncoder endEncoding];

    // Execute the command.
    [commandBuffer commit];

    // Normally, you want to do other work in your app while the GPU is running,
    // but in this example, the code simply blocks until the calculation is complete.
    [commandBuffer waitUntilCompleted];
    
    // Compute our estimate
    float* result = _mBufferResult.contents;
    Float64 total = 0;
    for (int i = 0; i < arrayLength; i++) {
//        if (result[i] != 0) {
//            NSLog(@"%f", result[i]);
//        }
        total += result[i];
    }
    NSLog(@"pi estimate: %f", total * 4 / (arrayLength * 1000));

//    [self verifyResults];
}

- (void)encodeEstimateCommand:(id<MTLComputeCommandEncoder>)computeEncoder {

    // Encode the pipeline state object and its parameters.
    [computeEncoder setComputePipelineState:_mEstimateFunctionPSO];
    [computeEncoder setBuffer:_mBufferResult offset:0 atIndex:0];

    // Define the dimentions of the grid
    MTLSize gridSize = MTLSizeMake(arrayLength, 1, 1);

    // Set the threadgroup size.
    NSUInteger threadGroupSize = _mEstimateFunctionPSO.maxTotalThreadsPerThreadgroup;
    if (threadGroupSize > arrayLength)
    {
        threadGroupSize = arrayLength;
    }
    MTLSize threadgroupSize = MTLSizeMake(threadGroupSize, 1, 1);

    // Encode the compute command.
    [computeEncoder dispatchThreads:gridSize
              threadsPerThreadgroup:threadgroupSize];
}

//- (void) generateRandomFloatData: (id<MTLBuffer>) buffer
//{
//    float* dataPtr = buffer.contents;
//
//    for (unsigned long index = 0; index < arrayLength; index++)
//    {
//        dataPtr[index] = (float)rand()/(float)(RAND_MAX);
//    }
//}
//- (void) verifyResults
//{
//    float* a = _mBufferA.contents;
//    float* b = _mBufferB.contents;
//    float* result = _mBufferResult.contents;
//
//    for (unsigned long index = 0; index < arrayLength; index++)
//    {
//        if (result[index] != (a[index] + b[index]))
//        {
//            printf("Compute ERROR: index=%lu result=%g vs %g=a+b\n",
//                   index, result[index], a[index] + b[index]);
//            assert(result[index] == (a[index] + b[index]));
//        }
//    }
//    printf("Compute results as expected\n");
//}
@end
