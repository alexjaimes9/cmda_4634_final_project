/*
See LICENSE folder for this sample’s licensing information.

Abstract:
An app that performs a simple calculation on a GPU.
*/

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import "MetalCalculator.h"

// This is the C version of the function that the sample implements in Metal Shading Language.
//void add_arrays(const float* inA,
//                const float* inB,
//                float* result,
//                int length)
//{
//    for (int index = 0; index < length ; index++)
//    {
//        result[index] = inA[index] + inB[index];
//    }
//}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // Get the default device detected by the system
        id<MTLDevice> device = MTLCreateSystemDefaultDevice();
        
        // Print the name of the device
        NSLog(@"%@", device.name);

        // Create the custom object used to encapsulate the Metal code.
        // Initializes objects to communicate with the GPU.
        MetalCalculator* calculator = [[MetalCalculator alloc] initWithDevice:device];
        
        // Create buffers to hold data
        [calculator prepareData];
        
        // Send a command to the GPU to perform the calculation.
        [calculator sendComputeCommand];
        
        // NSLog is print but also adds a time stamp
        NSLog(@"Execution finished");
    }
    return 0;
}
